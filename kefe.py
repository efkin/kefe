#!/usr/bin/python3
# Scrapes the content of a user from a given platform

from argparse import ArgumentParser
from bs4 import BeautifulSoup as bs
from requests import get, post, Session
import re

import readers.instagram
import writers.instagram


# ACTION!

if __name__ == '__main__':
    # Argument parser initialization
    parser = ArgumentParser()
    parser.add_argument("-r", "--reader", dest="reader", help="The platform reader that you need. For a list of available readers use the '-l' option.")
    parser.add_argument("-u", "--username", dest="username", help="The username that you want to scrape")
    parser.add_argument("-l", "--list", dest="show_list", action="store_true", help="Print a list of available readers")

    args = parser.parse_args()

    # print readers and exit
    if args.show_list:
        print("Available readers:")
        [print(x) for x in dir(readers) if not re.match("__", x)]
        exit(0)
